<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/about', function () {
    return "this is me!";
});
Route::get('admin/login', 'AdminController@login_form');
Route::get('/{locale?}', 'AdminController@home');
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => ['admin']], function () {
    
	Route::get('admin', function () {
	    return Redirect::to('admin/dashboard/');
	});
	Route::get('admin/dashboard', 'AdminController@index');
	Route::get('admin/dashboard/file', 'AdminController@file');
	Route::get('admin/topic', 'AdminController@pages');
	Route::get('admin/topic/edit/{id}', 'AdminController@edit');
	Route::get('admin/topic/ajax', 'AdminController@ajax');
});
// Route::get('/cache', function () {
	