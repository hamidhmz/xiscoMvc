<?php

return [

	
    'howitswork' => 'از طراحی تا <span class="">اجرا</span>',
    'designportfolio'  => 'نمونه کار طراحی',
    'viewproject'  => 'مشاهده نمونه',
    'sitesupportandbasechange'  => 'پشتیبانی سایت و تغییرات بنیادی',
    'portfolio' => 'نمونه کار',
    'changeable' => 'انطباق پذیری و واکنش گرایی',
    'hamidrezanasrollahy' => 'حمید نصرالهی',
    'backenddeveloper' => 'Backend Developer',
    'hamidknowledge' => 'مسلط به php ،SQL ،jQuery ،js ،Ruby on Rails و Laravel , رشته تحصیلی فناوری اطلاعات.',
    'miladghamati' => 'میلاد قامتی',
    'miladknowledge' => 'علاقه ی زیادی به گرافیک و طراحی دارد، رشته ی تحصیلی اش نرم افزار است و به طرح های غیر متعارف و جدید تمایل بیشتری دارد ،و طرح های نبوغ آمیز او را شگقت زده میکند.',
    'uxdesigner' => 'UX Designer',
];