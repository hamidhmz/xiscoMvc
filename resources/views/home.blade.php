<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Xisco Design - Website Design - Web Develope </title>
<meta name="keywords" content="طراحی وبسایت,طراحی وب سایت, طراحی سایت , طراحی لوگو , طراحی فروشگاه اینترنتی , طراحی , فروشگاه آنلاین , فروشگاه مجازی , html , php , گرافیک , seo , jquery,javascript,XML,سایت,وبسایت,وردپرس,wordpress">
<meta name="description" content="شرکت زیسکو با ارائه خدمات اینترنتی از جمله طراحی وبسایت توسعه ی دیتابیس ، فروشگاه های اینترنتی ">
<meta name="author" content="Milad Ghamati">
<link rel="shortcut icon" href="{{ URL::asset('public/assets/images/xisco-favi.ico') }}" type="image/x-icon">
<link href="{{ URL::asset('public/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('public/css/animate.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('public/css/hover.css') }}" rel="stylesheet" media="all">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
<meta name="viewport" content="width=device-wissdth, initial-scale=1">
<meta name="description" content="Web Designer And Developer In Iran">
<meta name="author" content="Milad Ghamati">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="{{ URL::asset('public/js/jquery.debouncedresize.js') }}"></script>
<script src="{{ URL::asset('public/js/waypoints.min.js') }}"></script>
<script src="{{ URL::asset('public/css/loader.css') }}"></script>

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato&subset=latin">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script>initComponents = [
    { name: 'shapes', data: { url: '/sprites/shapes.svg' } }
]
</script>

</head>
<body>
<div id="preloader" style="">
        <div id="loading-animation loader" style="">
            <div class="loader JS_on">
              <span class="binary"></span>
              <span class="binary"></span>
              <span class="getting-there">LOADING STUFF...</span>
            </div>
        </div>
    </div>
<div class="whole">
 
 <div id="cbp-fbscroller" class="cbp-fbscroller">
                <nav>
                    <a href="#section1" class="cbp-fbcurrent"></a>
                    <a href="#section2" class=""></a>
                    <a href="#section3" class=""></a>
                    <a href="#section4"></a>
                    <a href="#section5"></a>
                    <a href="#section6"></a>
                    <a href="#section7"></a>
                </nav>
                <section id="section1" class="intro">
  <div id="introWiring" class="introWiring">
    <canvas class="canvas is-visible" width="1772" height="790" style="width: 1418px; height: 632px;"></canvas>
  </div>
  <script>initComponents.push({
    name: 'introWiring',
    place: '#introWiring',
    data: {
        canvas: '.canvas',
        config: {
            NEURON_HEALTH_WEAROFF_DURATION: 1200,
            NEURONS_COUNT: 77,
            SYNAPSIS: [[1,0],[2,1],[3,1],[3,2],[4,2],[4,3],[5,4],[5,3],[6,3],[7,6],[7,3],[7,5],[8,5],[9,5],[9,8],[10,9],[10,8],[11,8],[11,10],[12,11],[13,12],[13,11],[14,10],[15,14],[15,11],[16,15],[17,16],[18,16],[18,17],[19,15],[19,16],[19,17],[20,19],[20,11],[20,13],[21,20],[22,20],[22,21],[23,21],[23,13],[24,23],[24,13],[25,24],[26,24],[26,25],[27,26],[28,27],[28,26],[29,28],[30,29],[30,0],[32,31],[33,32],[34,33],[34,32],[35,34],[35,32],[36,32],[36,35],[36,4],[36,2],[37,36],[37,4],[37,1],[38,32],[38,36],[39,37],[39,1],[39,38],[27,30],[27,25],[27,12],[40,27],[40,12],[40,13],[40,25],[25,13],[8,12],[6,1],[6,0],[41,7],[41,8],[41,6],[42,8],[42,41],[42,6],[42,27],[42,30],[42,0],[43,30],[43,29],[43,0],[44,0],[45,44],[45,0],[45,39],[46,1],[46,0],[46,45],[46,39],[47,39],[47,45],[48,38],[48,39],[48,32],[48,31],[49,36],[50,49],[51,49],[51,50],[52,50],[52,51],[53,49],[53,51],[53,36],[53,4],[53,9],[53,10],[54,10],[54,53],[54,51],[54,52],[54,14],[55,51],[55,15],[55,14],[55,54],[56,52],[57,55],[56,55],[56,57],[57,18],[58,18],[58,17],[59,22],[59,17],[59,58],[60,59],[60,22],[60,23],[61,60],[62,60],[62,61],[63,62],[64,63],[65,64],[66,65],[66,64],[66,63],[67,62],[67,63],[67,65],[67,64],[68,65],[68,67],[69,68],[70,69],[70,24],[71,70],[72,71],[72,70],[72,26],[73,26],[73,72],[73,70],[73,24],[70,68],[74,70],[74,67],[74,68],[75,70],[75,24],[75,74],[75,67],[75,23],[75,60],[76,60],[76,75],[76,67],[76,62]],
            NEURON_POSITION: function(i) {
                return [[0.4217207334273625,0.1787974683544304],[0.34203102961918197,0.3449367088607595],[0.33779971791255287,0.47468354430379744],[0.3716502115655853,0.4477848101265823],[0.3843441466854725,0.5632911392405063],[0.4217207334273625,0.5791139240506329],[0.4083215796897038,0.3528481012658228],[0.41607898448519043,0.4699367088607595],[0.49647390691114246,0.46677215189873417],[0.4308885754583921,0.6265822784810127],[0.4894217207334274,0.6566455696202531],[0.5289139633286318,0.625],[0.535966149506347,0.504746835443038],[0.6001410437235543,0.5],[0.5007052186177715,0.7072784810126582],[0.5437235543018336,0.7579113924050633],[0.5430183356840621,0.8291139240506329],[0.5867418899858956,0.8291139240506329],[0.5768688293370945,0.8813291139240507],[0.581805359661495,0.7278481012658228],[0.5952045133991537,0.6629746835443038],[0.6382228490832158,0.5886075949367089],[0.6318758815232722,0.6550632911392406],[0.6622002820874471,0.5791139240506329],[0.6671368124118476,0.40189873417721517],[0.6050775740479548,0.33860759493670883],[0.5909732016925247,0.18670886075949367],[0.5296191819464033,0.2927215189873418],[0.5423131170662906,0.18037974683544303],[0.5232722143864598,0.1550632911392405],[0.4795486600846262,0.17246835443037975],[0.04160789844851904,0.495253164556962],[0.1368124118476728,0.6629746835443038],[0.01692524682651622,0.7136075949367089],[0.11636107193229901,0.7848101265822784],[0.21932299012693934,0.865506329113924],[0.27150916784203105,0.6218354430379747],[0.22778561354019747,0.4050632911392405],[0.1777150916784203,0.4699367088607595],[0.21086036671368125,0.31962025316455694],[0.5712270803949224,0.4177215189873418],[0.4506346967559944,0.40822784810126583],[0.49647390691114246,0.31962025316455694],[0.47249647390691113,0.08069620253164557],[0.3815232722143865,0.04746835443037975],[0.28138222849083216,0.15031645569620253],[0.3131170662905501,0.24841772151898733],[0.17842031029619182,0.12025316455696203],[0.11283497884344147,0.3575949367088608],[0.35613540197461213,0.7879746835443038],[0.37799717912552894,0.9889240506329114],[0.40409026798307474,0.8354430379746836],[0.4527503526093089,0.8829113924050633],[0.40479548660084624,0.7072784810126582],[0.4590973201692525,0.7341772151898734],[0.5056417489421721,0.8069620253164557],[0.4781382228490832,0.9825949367088608],[0.5366713681241185,0.9098101265822784],[0.6234132581100141,0.8876582278481012],[0.6276445698166432,0.7958860759493671],[0.6988716502115656,0.7294303797468354],[0.7031029619181947,0.8971518987341772],[0.8152327221438646,0.615506329113924],[0.8589562764456982,0.6756329113924051],[0.8624823695345557,0.44145569620253167],[0.8638928067700987,0.39082278481012656],[0.9689703808180536,0.3560126582278481],[0.8258110014104373,0.44145569620253167],[0.8497884344146686,0.25949367088607594],[0.7870239774330042,0.15664556962025317],[0.7256699576868829,0.22468354430379747],[0.7080394922425952,0.07278481012658228],[0.6572637517630465,0.08227848101265822],[0.6636107193229901,0.20569620253164558],[0.7905500705218618,0.3670886075949367],[0.7320169252468265,0.46835443037974683],[0.7708039492242595,0.5443037974683544]][i]
            },
            UNDEAD_NEURONS: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
            UNDEAD_SYNAPSIS: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50]
        }
    }
})</script>
 <div class="icon-holder-top">
    <div class="telegram-icon">
        <a href="https://www.telegram.me/@MiladGhamati" class="telegram-layout"><img class="telegram-layout-img-telegram" src="{{ URL::asset('public/assets/images/telegram.png') }}" width="35" height="35" alt="telegram"></a>
    </div>
    <div class="instagram-icon">
        <a href="https://www.instagram.com/milad_gmt/" class="instagram-layout"><img class="telegram-layout-img-instagram" src="{{ URL::asset('public/assets/images/instagram.png') }}" width="35" height="35" alt="telegram"></a>
    </div>
</div>
  <header class="">
  <div style="margin-top: -25px;">

        <div>

            <header class="home-header">
                <div class="inner-home-header">
                    <div class="home-header-content">
                        <div id="elem" class="move-wrap">
                            <h1 style="transform: translate(-2.08px, 1.085px);" class="h1">Xisco</h1>
                            <p style="transform: translate(-2.08px, 1.085px);" class="h1-2">DESIGN</p>
                            <div class="line-wrap" style="transform: translate(2.08px, -1.085px);">
                                <span class="line"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

        </div>

    </div>

 <?php
 $locale = App::getLocale();
    if (App::isLocale('en')) {?>
<div><a class="lang lang-en" href="{{url('/')}}"><img src="{{ URL::asset('public/assets/images/FA-icon.png') }}" width="40px" height="40px" title="Language : Persian"></a>
 </div>
<?php
    }   
 ?>
 <?php
    if (App::isLocale('fa')) {?>
<div><a class="lang lang-en" href="{{url('en')}}"><img src="{{ URL::asset('public/assets/images/EN-icon.png') }}" width="40px" height="40px" title="Language : English"></a>
 </div>
<?php
    }   
 ?>
 
 </header>
 </section>
 <section class="" id="section2" >
     <div class="text">
      <h1 class="wow fadeInLeft animated bio-h1" data-wow-duration="1.0s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.4s; animation-name: fadeInLeft;">{!! __('home.howitswork') !!}</h1>
      <hr class="wow fadeInUp animated bio-hr " data-wow-duration="1.0s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.6s; animation-name: fadeInUp;">
      <div style="clear:both"></div>
      <div class="wow fadeInUp animated bio-passage" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;">
      <p><?php if($locale == 'fa') {echo textecho(1,$texts,$locale);}else{echo textecho(2,$texts,$locale);} ?></p>
     </div>
    
    </div>
    
    <div class="mobileinfo">
                        
                </div>
                
  </section>
  
  <section id="section3">
  <div class="wow fadeInUp animated role-shopisoft" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a class="svg-href2" title="Milad Ghamati"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
     viewBox="0 0 202.8 174.7" width="100px" height="100px">

<g>
    <path class="st0" d="M192,124.1c3.4,5.5,5.2,11.1,5.2,16.8c0,7-2.4,12.9-7.2,17.9c-4.8,4.9-10.7,7.4-17.8,7.4H31.3
        c-7.1,0-13-2.5-17.8-7.4S6.3,148,6.3,141c0-5.7,1.7-11.3,5.2-16.8L76.6,21.5c6.2-9.9,14.6-14.8,25.1-14.8
        c10.4,0,18.8,4.9,25.1,14.8L192,124.1z M168,120.9l-48.2-75c-7.6-11.9-13.6-17.9-18-17.9c-4.5,0-10.5,6-18.1,17.9l-48.1,75
        c-5.6,8.9-8.4,15.4-8.4,19.7c0,6.3,6.3,9.5,19,9.5h111.3c12.7,0,19-3.2,19-9.5C176.4,136.3,173.6,129.7,168,120.9z"/>
</g>
</svg>
<span class="my-role">Creator:</span>
</a>
     </div>
  <div class="info">
     <div class="text">
      <h1 class="wow fadeInLeft animated shopisoft-h1" data-wow-duration="1.0s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.4s; animation-name: fadeInLeft;">SHOPISOFT</h1>
      <h2 class="wow fadeInLeft animated shopisoft-h2" data-wow-duration="1.0s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInLeft;">{!! __('home.designportfolio') !!}</h2>
      <hr class="wow fadeInLeft animated shopisoft-hr" data-wow-duration="1.0s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.6s; animation-name: fadeInLeft;">
      <div style="clear:both"></div>
      <div class="wow fadeInLeft animated shopisoft-passage" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;">
          <p><?php if($locale == 'fa') {echo textecho(3,$texts,$locale);}else{echo textecho(4,$texts,$locale);} ?>
          </p>
     </div>
     <div class="wow fadeInLeft animated shopisoft-passage shopisoft-buttom" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;"><a href="http://www.shopisoft.com/" target="_blank" class="hvr-sweep-to-left">{!! __('home.viewproject') !!}</a>
     </div>
    </div>
    </div>
    <div class="mobileinfo">
                        <h1 class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.7s; animation-name: fadeInUp;">Shopisoft</h1>
                        <div class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a id="" href="http://www.shopisoft.com/" class="shopisoft-buttom ">{!! __('home.viewproject') !!}</a></div>
                </div>
  </section>
  <section id="section4">
  <div class="wow fadeInUp animated role-personal" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a class="svg-href2" title="Milad Ghamati"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
     viewBox="0 0 202.8 174.7" width="100px" height="100px">

<g>
    <path class="st0" d="M192,124.1c3.4,5.5,5.2,11.1,5.2,16.8c0,7-2.4,12.9-7.2,17.9c-4.8,4.9-10.7,7.4-17.8,7.4H31.3
        c-7.1,0-13-2.5-17.8-7.4S6.3,148,6.3,141c0-5.7,1.7-11.3,5.2-16.8L76.6,21.5c6.2-9.9,14.6-14.8,25.1-14.8
        c10.4,0,18.8,4.9,25.1,14.8L192,124.1z M168,120.9l-48.2-75c-7.6-11.9-13.6-17.9-18-17.9c-4.5,0-10.5,6-18.1,17.9l-48.1,75
        c-5.6,8.9-8.4,15.4-8.4,19.7c0,6.3,6.3,9.5,19,9.5h111.3c12.7,0,19-3.2,19-9.5C176.4,136.3,173.6,129.7,168,120.9z"/>
</g>
</svg>
<span class="my-role-xisco">Creator:</span>
</a>
     </div>
  <div class="info">
     <div class="text">
      <h1 class="wow fadeInLeft animated lamp-h1" data-wow-duration="1.0s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.4s; animation-name: fadeInLeft;">Xisco V.1</h1>
      <h2 class="wow fadeInLeft animated lamp-h2 " data-wow-duration="1.0s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInLeft;">{!! __('home.sitesupportandbasechange') !!}</h2>
      <hr class="wow fadeInLeft animated lamp-hr" data-wow-duration="1.0s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.6s; animation-name: fadeInLeft;">
      <div style="clear:both"></div>
      <div class="wow fadeInLeft animated lamp-passage info-sec5" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;">
          <p><?php if($locale == 'fa') {echo textecho(5,$texts,$locale);}else{echo textecho(6,$texts,$locale);} ?>
          </p>
     </div>
    </div>
    </div>
    <div class="mobileinfo mobileinfo-xisco">
                        <h1 class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.7s; animation-name: fadeInUp;">Xisco V.1</h1>
                        
                    </div>  
  </section>
  <section id="section5">
  <div class="wow fadeInUp animated role-tmp" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a class="svg-href3" title="Hamid Nasrolahi"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100px" height="100px"
     viewBox="0 0 202.8 174.7">
<g>
    <path d="M21.7,86.9c0-17.6,7.7-32.7,23.1-45.1c15.4-12.4,34.1-18.7,55.9-18.7c21.9,0,40.5,6.2,55.9,18.7
        c15.4,12.4,23.1,27.5,23.1,45.1c0,17.6-7.7,32.7-23.1,45.1c-15.4,12.4-34.1,18.7-55.9,18.7c-21.9,0-40.5-6.2-55.9-18.7
        C29.4,119.6,21.7,104.6,21.7,86.9z M37.5,86.9c0,14.1,6.2,26.1,18.5,36c12.4,10,27.3,14.9,44.7,14.9c17.5,0,32.4-5,44.7-14.9
        c12.4-10,18.5-22,18.5-36s-6.2-26.1-18.5-36c-12.4-10-27.3-14.9-44.7-14.9c-17.5,0-32.4,5-44.7,14.9C43.7,60.8,37.5,72.9,37.5,86.9
        z"/>
</g>
</svg>
<span class="my-role">Backend:</span>
</a>
     </div>
  <div class="info">
     <div class="text">
      <h1 class="wow fadeInLeft animated aboutme-h1" data-wow-duration="1.0s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.4s; animation-name: fadeInLeft;">TmpIran</h1>
      <h2 class="wow fadeInLeft animated aboutme-h2 " data-wow-duration="1.0s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInLeft;">{!! __('home.portfolio') !!}</h2>
      <hr class="wow fadeInLeft animated aboutme-hr" data-wow-duration="1.0s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.6s; animation-name: fadeInLeft;">
      <div style="clear:both"></div>
      <div class="wow fadeInLeft animated aboutme-passage" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;">
          <p><?php if($locale == 'fa') {echo textecho(7,$texts,$locale);}else{echo textecho(8,$texts,$locale);} ?>
          </p>
     </div>
     <div class="wow fadeInLeft animated shopisoft-passage aboutme-buttom" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;"><a href="http://www.shopisoft.com/" target="_blank" class="hvr-sweep-to-left">{!! __('home.viewproject') !!}</a>
     </div>
    </div>
    </div>
    <div class="mobileinfo">
                        <h1 class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.7s; animation-name: fadeInUp;">TmpIran</h1>
                        <div class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a id="" href="https://tmpiran.com/" class="tmp-buttom">{!! __('home.viewproject') !!}</a></div>
                    </div>  
  </section>
  <section id="section6">
  <div class="wow fadeInUp animated role-avada" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a class="svg-href4" title="Milad Ghamati"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
     viewBox="0 0 202.8 174.7" width="100px" height="100px">

<g>
    <path class="st0" d="M192,124.1c3.4,5.5,5.2,11.1,5.2,16.8c0,7-2.4,12.9-7.2,17.9c-4.8,4.9-10.7,7.4-17.8,7.4H31.3
        c-7.1,0-13-2.5-17.8-7.4S6.3,148,6.3,141c0-5.7,1.7-11.3,5.2-16.8L76.6,21.5c6.2-9.9,14.6-14.8,25.1-14.8
        c10.4,0,18.8,4.9,25.1,14.8L192,124.1z M168,120.9l-48.2-75c-7.6-11.9-13.6-17.9-18-17.9c-4.5,0-10.5,6-18.1,17.9l-48.1,75
        c-5.6,8.9-8.4,15.4-8.4,19.7c0,6.3,6.3,9.5,19,9.5h111.3c12.7,0,19-3.2,19-9.5C176.4,136.3,173.6,129.7,168,120.9z"/>
</g>
</svg>
<span class="my-role-xisco">Creator:</span>
</a>
     </div>
  <div class="info">
     <div class="text">
      <h1 class="wow fadeInLeft animated games-h1" data-wow-duration="1.0s" data-wow-delay="0.5s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.4s; animation-name: fadeInLeft;">Avada</h1>
      <h2 class="wow fadeInLeft animated games-h2 " data-wow-duration="1.0s" data-wow-delay="0.6s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.5s; animation-name: fadeInLeft;">{!! __('home.sitesupportandbasechange') !!}</h2>
      <hr class="wow fadeInLeft animated games-hr" data-wow-duration="1.0s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.6s; animation-name: fadeInLeft;">
      <div style="clear:both"></div>
      <div class="wow fadeInLeft animated games-passage " data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;">
          <p><?php if($locale == 'fa') {echo textecho(7,$texts,$locale);}else{echo textecho(8,$texts,$locale);} ?>
          </p>
     </div>
     <div class="wow fadeInLeft animated shopisoft-passage games-buttom" data-wow-duration="1.0s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInLeft;"><a href="html/Links/Avada/index.html" target="_blank" class="hvr-sweep-to-left">{!! __('home.viewproject') !!}</a>
     </div>
    </div>
    </div>
    <div class="mobileinfo mobileinfo-avada">
                        <h1 class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.7s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.7s; animation-name: fadeInUp;">Avada</h1>
                        <div class="wow fadeInUp animated" data-wow-duration="0.6s" data-wow-delay="0.8s" style="visibility: visible; animation-duration: 0.6s; animation-delay: 0.8s; animation-name: fadeInUp;"><a id="" href="html/Links/Avada/index.html" class="avada-buttom">{!! __('home.viewproject') !!}</a></div>
                    </div>  
  </section>
  <section id="section9">
    <div class="team-bg">
    <div class="team">
  <div class="team-block">
    <div class="team-members">
      <div class="team-member">
        <div class="team-member-in">
          <div class="team-member-image">
            <div class="team-member-image-wrap"><img src="{{ URL::asset('public/assets/images/milad-photo.png') }}" width="150px" height="140px" class="team-member-image-img"><img src="{{ URL::asset('public/assets/images/mld.png') }}" width="63px" height="53px" class="team-member-symbol"></div>
          </div>
          <div class="team-member-content">
            <div class="team-member-name">{!! __('home.miladghamati') !!}</div>
            <div class="team-member-position">UX Designer</div>
            <div class="team-member-text">{!! __('home.miladknowledge') !!}</div>
          </div>
        </div>
      </div>
      <div class="team-member">
        <div class="team-member-in">
          <div class="team-member-image">
            <div class="team-member-image-wrap"><img src="{{ URL::asset('public/assets/images/hamid-photo.png') }}" width="150px" height="140px" class="team-member-image-img"><img src="{{ URL::asset('public/assets/images/hmd.png') }}" width="63px" height="53px" class="team-member-symbol"></div>
          </div>
          <div class="team-member-content">
            <div class="team-member-name">{!! __('home.hamidrezanasrollahy') !!}</div>
            <div class="team-member-position">{!! __('home.backenddeveloper') !!}</div>
            <div class="team-member-text">{!! __('home.hamidknowledge') !!}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
 </div>
 <!--<img class="arrow-dw" src="image/arrow.png" width="110" height="53" alt="Arrow">-->
</div>
    </section>
    
    <div id="social-media" class="clearfix social-media">
        <main class="container">
                                            <span class="social-media__item single-sm_slug_facebook">
                    <a href="https://www.facebook.com/milad.ghamati.7" class="social-media__item-href sm_slug_facebook" title="Facebook">
                        <figure class="social-media__item-figure">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13.03" height="23"><path id="facebook_svg_icon" d="M8.67 23H2.9V12.12H0V7.95h2.9v-2.5C2.9 2.02 4.53 0 9.16 0H13v4.18h-2.4c-1.8 0-1.92.6-1.92 1.68v2.1h4.36l-.5 4.17H8.66V23z" class="svg-single-item svg-single-item-facebook"></path></svg>                     </figure>
                        <h5 class="social-media__item-title">Facebook</h5>
                    </a>
                </span>
                                            <span class="social-media__item single-sm_slug_instagram">
                    <a href="https://www.instagram.com/milad_gmt/" class="social-media__item-href sm_slug_instagram" title="Instagram">
                        <figure class="social-media__item-figure">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path id="instagram_svg_icon" d="M23.93 16.95a8.87 8.87 0 0 1-.56 2.9 5.82 5.82 0 0 1-1.4 2.13 5.9 5.9 0 0 1-2.1 1.4 9.03 9.03 0 0 1-2.92.55c-1.28.05-1.7.07-4.95.07s-3.67-.02-4.95-.07a9.03 9.03 0 0 1-2.9-.56A5.9 5.9 0 0 1 2 21.97a5.78 5.78 0 0 1-1.37-2.1 8.8 8.8 0 0 1-.56-2.92C0 15.67 0 15.25 0 12s0-3.67.07-4.95a8.74 8.74 0 0 1 .56-2.9A5.9 5.9 0 0 1 2 2 6 6 0 0 1 4.15.64a8.77 8.77 0 0 1 2.9-.56C8.35 0 8.75 0 12 0s3.67 0 4.95.07a8.77 8.77 0 0 1 2.9.56A6.02 6.02 0 0 1 22 2a5.94 5.94 0 0 1 1.4 2.14 8.82 8.82 0 0 1 .55 2.9c.05 1.3.07 1.7.07 4.96s-.02 3.67-.07 4.95zm-2.16-9.8a6.58 6.58 0 0 0-.42-2.23 3.72 3.72 0 0 0-.9-1.38 3.76 3.76 0 0 0-1.38-.9 6.67 6.67 0 0 0-2.23-.4c-1.26-.07-1.64-.08-4.85-.08s-3.6 0-4.86.07a6.7 6.7 0 0 0-2.22.4 4 4 0 0 0-2.28 2.3 6.64 6.64 0 0 0-.4 2.22C2.16 8.42 2.15 8.8 2.15 12s0 3.58.07 4.85a6.7 6.7 0 0 0 .4 2.23 3.74 3.74 0 0 0 .9 1.38 3.6 3.6 0 0 0 1.4.9 6.63 6.63 0 0 0 2.22.4c1.26.06 1.64.08 4.85.08s3.58-.02 4.84-.07a6.6 6.6 0 0 0 2.23-.42 3.6 3.6 0 0 0 1.38-.9 3.72 3.72 0 0 0 .9-1.37 6.64 6.64 0 0 0 .42-2.23c.06-1.27.07-1.65.07-4.85s-.02-3.58-.08-4.85zm-3.37-.12a1.44 1.44 0 1 1 1.44-1.44 1.44 1.44 0 0 1-1.44 1.43zM12 18.16A6.16 6.16 0 1 1 18.16 12 6.16 6.16 0 0 1 12 18.16zM12 8a4 4 0 1 0 4 4 4 4 0 0 0-4-4z" class="svg-single-item svg-single-item-instagram"></path></svg>                      </figure>
                        <h5 class="social-media__item-title">Instagram</h5>
                    </a>
                </span>
                                            <span class="social-media__item single-sm_slug_youtube">
                    <a href="https://plus.google.com/u/0/106790720111947270200" class="social-media__item-href sm_slug_youtube" title="Google Plus">
                        <figure class="social-media__item-figure">
                            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="25" x="0px" y="0px"
     viewBox="0 0 56.7 56.7"><path id="youtube_svg_icon" d="M52.2,25.9h-7.5v-7.5c0-0.6-0.5-1-1-1h-2.1c-0.6,0-1,0.5-1,1v7.5H33c-0.6,0-1,0.5-1,1V29c0,0.6,0.5,1,1,1h7.5v7.5
        c0,0.6,0.5,1,1,1h2.1c0.6,0,1-0.5,1-1v-7.5h7.5c0.6,0,1-0.5,1-1v-2.1C53.3,26.3,52.8,25.9,52.2,25.9z" class="svg-single-item svg-single-item-youtube"></path><path id="youtube_svg_icon" d="M27,32.4c-1.6-1.1-3-2.8-3-3.3c0-0.9,0.1-1.4,2.1-3c2.7-2.1,4.1-4.8,4.1-7.7c0-2.6-0.8-5-2.2-6.6h1.1
        c0.2,0,0.4-0.1,0.6-0.2l3-2.1C33,9.3,33.2,8.8,33,8.4c-0.1-0.4-0.5-0.7-1-0.7H18.8c-1.4,0-2.9,0.3-4.4,0.8c-4.8,1.7-8.2,5.8-8.2,10
        c0,6,4.6,10.5,10.8,10.6c-0.1,0.5-0.2,0.9-0.2,1.4c0,0.9,0.2,1.8,0.7,2.6c-0.1,0-0.1,0-0.2,0c-5.9,0-11.2,2.9-13.2,7.2
        c-0.5,1.1-0.8,2.3-0.8,3.4c0,1.1,0.3,2.1,0.8,3.1c1.3,2.3,4,4,7.7,5c1.9,0.5,3.9,0.7,6.1,0.7c1.9,0,3.7-0.2,5.4-0.7
        c5.2-1.5,8.6-5.4,8.6-9.8C32,37.6,30.7,35,27,32.4z M10.3,42.2c0-3.1,3.9-5.8,8.4-5.8h0.1c1,0,1.9,0.2,2.8,0.4
        c0.3,0.2,0.6,0.4,0.9,0.6c2.1,1.4,3.5,2.4,3.8,3.9c0.1,0.4,0.1,0.8,0.1,1.1c0,3.9-2.9,5.8-8.6,5.8C13.7,48.2,10.3,45.6,10.3,42.2z
         M14.4,12.9c0.7-0.8,1.6-1.2,2.7-1.2l0.1,0c2.9,0.1,5.7,3.3,6.2,7.3c0.3,2.2-0.2,4.3-1.3,5.5c-0.7,0.8-1.6,1.2-2.7,1.2c0,0,0,0,0,0
        h0c-2.9-0.1-5.7-3.5-6.2-7.4C12.8,16.1,13.3,14.1,14.4,12.9z" class="svg-single-item svg-single-item-youtube"></path></svg>                       </figure>
                        <h5 class="social-media__item-title">Google+</h5>
                    </a>
                </span>
                                            <span class="social-media__item single-sm_slug_twitter">
                    <a href="https://twitter.com/GhamatiMilad" class="social-media__item-href sm_slug_twitter" title="Twitter">
                        <figure class="social-media__item-figure">
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="25"><path id="twitter_svg_icon" d="M30 2.96a12.03 12.03 0 0 1-3.54 1 6.27 6.27 0 0 0 2.7-3.5A11.9 11.9 0 0 1 25.27 2a6.1 6.1 0 0 0-4.5-2 6.24 6.24 0 0 0-6.14 6.3 6.45 6.45 0 0 0 .16 1.45 17.34 17.34 0 0 1-12.7-6.6A6.42 6.42 0 0 0 4 9.6a6 6 0 0 1-2.8-.8v.1a6.28 6.28 0 0 0 4.95 6.18 6.2 6.2 0 0 1-1.63.22 6.3 6.3 0 0 1-1.15-.1 6.2 6.2 0 0 0 5.75 4.37 12.17 12.17 0 0 1-7.65 2.7 11.9 11.9 0 0 1-1.47-.1A17.12 17.12 0 0 0 9.44 25c11.32 0 17.5-9.62 17.5-17.96 0-.27 0-.54 0-.8A12.62 12.62 0 0 0 30 2.95z" class="svg-single-item svg-single-item-twitter"></path></svg>                       </figure>
                        <h5 class="social-media__item-title">Twitter</h5>
                    </a>
                </span>
                                            <span class="social-media__item single-sm_slug_tripadvisor">
                    <a href="https://t.me/MiladGhamati" class="social-media__item-href sm_slug_tripadvisor" title="Telegram">
                        <figure class="social-media__item-figure">
                            <svg xmlns="http://www.w3.org/2000/svg" width="26.97" height="22"><path id="telegram_svg_icon" d="M22.05 1.577c-.393-.016-.784.08-1.117.235-.484.186-4.92 1.902-9.41 3.64-2.26.873-4.518 1.746-6.256 2.415-1.737.67-3.045 1.168-3.114 1.192-.46.16-1.082.362-1.61.984-.133.155-.267.354-.335.628s-.038.622.095.895c.265.547.714.773 1.244.976 1.76.564 3.58 1.102 5.087 1.608.556 1.96 1.09 3.927 1.618 5.89.174.394.553.54.944.544l-.002.02s.307.03.606-.042c.3-.07.677-.244 1.02-.565.377-.354 1.4-1.36 1.98-1.928l4.37 3.226.035.02s.484.34 1.192.388c.354.024.82-.044 1.22-.337.403-.294.67-.767.795-1.307.374-1.63 2.853-13.427 3.276-15.38l-.012.046c.296-1.1.187-2.108-.496-2.705-.342-.297-.736-.427-1.13-.444zm-.118 1.874c.027.025.025.025.002.027-.007-.002.08.118-.09.755l-.007.024-.005.022c-.432 1.997-2.936 13.9-3.27 15.356-.046.196-.065.182-.054.17-.1-.015-.285-.094-.3-.1l-7.48-5.525c2.562-2.467 5.182-4.7 7.827-7.08.468-.235.39-.96-.17-.972-.594.14-1.095.567-1.64.84-3.132 1.858-6.332 3.492-9.43 5.406-1.59-.553-3.177-1.012-4.643-1.467 1.272-.51 2.283-.886 3.278-1.27 1.738-.67 3.996-1.54 6.256-2.415 4.522-1.748 9.07-3.51 9.465-3.662l.032-.013.03-.013c.11-.05.173-.055.202-.057 0 0-.01-.033-.002-.026zM10.02 16.016l1.234.912c-.532.52-1.035 1.01-1.398 1.36z" class="svg-single-item svg-single-item-tripadvisor"></path></svg>                     </figure>
                        <h5 class="social-media__item-title">Telegram</h5>
                    </a>
                </span>
            
                    </main>
    </div>
</div>
</div>
<script src="{{ URL::asset('public/js/main.min.js') }}"></script>
 <script src="{{ URL::asset('public/js/wow.min.js') }}"></script>
              <script>
              new WOW().init();
              
              $(window).load(function() {
        // will first fade out the loading animation
    jQuery("#loading-animation ,#loader").fadeOut();
            // will fade out the whole DIV that covers the website.
    jQuery("#preloader").delay(600).fadeOut("slow");
  
    
});
              </script>
<script> 
$(document).ready(function(){
$('.bio-buttom a').hover(function(){
//alert();
    $("#section3,#section4,#section5,#section6,#section7").addClass('hover');
}, function(){
    $(",#section3,#section4,#section5,#section6,#section7").removeClass('hover');
})
$('.shopisoft-buttom a').hover(function(){
//alert();
    $("#section3,#section4,#section5,#section6,#section7").addClass('hover');
}, function(){
    $("#section3,#section4,#section5,#section6,#section7").removeClass('hover');
})
$('.games-buttom a').hover(function(){
//alert();
    $("#section3,#section4,#section5,#section6,#section7").addClass('hover');
}, function(){
    $("#section3,#section4,#section5,#section6,#section7").removeClass('hover');
})
$('.lamp-buttom a').hover(function(){
//alert();
    $("#section3,#section4,#section5,#section6,#section7,#section8").addClass('hover');
}, function(){
    $("#section3,#section4,#section5,#section6,#section7,#section8").removeClass('hover');
})
$('.info-sec5').hover(function(){
//alert();
    $("#section3,#section4,#section5,#section6,#section7,#section8").addClass('hover');
}, function(){
    $("#section3,#section4,#section5,#section6,#section7,#section8").removeClass('hover');
})
$('.aboutme-buttom a').hover(function(){
//alert();
    $("#section3,#section4,#section5,#section6,#section7,#section8").addClass('hover');
}, function(){
    $("#section3,#section4,#section5,#section6,#section7,#section8").removeClass('hover');
})
      
});

</script>
<script>
            $(document).ready(function() {
                if($(window).width() > 768) {
                cbpFixedScrollLayout.init();
                }
            });
        </script>
         <div id="loader" class="pageload-overlay" data-opening="M20,15 50,30 50,30 30,30 Z;M0,0 80,0 50,30 20,45 Z;M0,0 80,0 60,45 0,60 Z;M0,0 80,0 80,60 0,60 Z" data-closing="M0,0 80,0 60,45 0,60 Z;M0,0 80,0 50,30 20,45 Z;M20,15 50,30 50,30 30,30 Z;M30,30 50,30 50,30 30,30 Z">
            <div id="move"></div>
            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 80 60" preserveAspectRatio="none">
                <path d="M30,30 50,30 50,30 30,30 Z"></path>
            </svg>
        </div>
        <script src="{{ URL::asset('public/js/1488989584-index.js') }}" defer=""></script>
        <script type="text/javascript" src="{{ URL::asset('public/js/main.min2.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('public/js/plugins.min.js') }}"></script>
</body>
</html>