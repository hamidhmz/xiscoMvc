@extends('admin.layout.layout')
@section('title', 'Dashboard')
@push('Tracks')
<li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
<li class="breadcrumb-item active">Home</li>
@endpush
@push('classhome')
active
@endpush
@push('classuphome')
active
@endpush
@push('classhomesidebar1')
 active open
@endpush
@push('classhomesidebar2')
show 
@endpush
@push('scripts')
<script src="{{ URL::asset('public/assets/vendor/admin/jquery.min.js') }}"></script>

@endpush