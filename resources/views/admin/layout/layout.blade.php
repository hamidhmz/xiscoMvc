<!DOCTYPE html>
<html class="theme-default" dir="ltr">


<!-- Mirrored from admindash.themekit.io/shop.html by HTTrack Website Copier/3.x [XR&CO'2010], Sun, 25 Jun 2017 19:54:21 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>


    <!-- Custom Vendor CSS -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-bootstrap-datepicker.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-bootstrap-datepicker.rtl.css') }}" rel="stylesheet">

    <!-- Custom Vendor CSS -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-morris.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-morris.rtl.css') }}" rel="stylesheet">


    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Simplebar -->
    <link type="text/css" href="{{ URL::asset('public/assets/vendor/admin/simplebar.css') }}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/app.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/app.rtl.css') }}" rel="stylesheet">

    <!-- Preloader CSS -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/preloader.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/preloader.rtl.css') }}" rel="stylesheet">

    <!-- Icons CSS -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-material-icons.rtl.css') }}" rel="stylesheet">
    <!-- <link type="text/css" href="css/themes/default/vendor-picto-icons.css" rel="stylesheet"> -->
    <!-- <link type="text/css" href="css/themes/default/vendor-picto-icons.rtl.css" rel="stylesheet"> -->

    <!-- App Settings (Demo) -->
    <!-- Safe to remove -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-fm-app-settings.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/vendor-fm-app-settings.rtl.css') }}" rel="stylesheet">

    <!-- Demo Utils -->
    <!-- Safe to remove -->
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/demo.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('public/assets/css/admin/demo.rtl.css') }}" rel="stylesheet">
    <link type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.css" rel="stylesheet">
    <link type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.semanticui.min.css" rel="stylesheet">
    <link type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.semanticui.min.css" rel="stylesheet">
    
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/> -->
    @stack('styles')
 <script src="{{ URL::asset('public/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('public/ckeditor/samples/js/sample.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('public/ckeditor/samples/css/samples.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}">

    

</head>

<body class="fullbleed-has-navbar-mini">
    <!-- loader -->
    <div class="preloader">
        <div class="sk-double-bounce">
            <div class="sk-child sk-double-bounce1"></div>
            <div class="sk-child sk-double-bounce2"></div>
        </div>
    </div>
    <!-- END loader -->

    <!-- upper navbar -->
    <div class="navbar navbar-full navbar-mini navbar-inverse bg-inverse-darken hidden-xs-down">
        <div class="container-fluid" style="padding-left: 1rem;">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" >Dashboard</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{url('admin/dashboard')}}" class="dropdown-item @stack('classuphome')">Home</a>
                        <a href="{{url('admin/dashboard/file')}}" class="dropdown-item @stack('classupfile')">file</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" >Topic</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{url('admin/topic')}}" class="dropdown-item @stack('classuppages')">List</a>
                    </div>
                </li>
            </ul>
            <!-- <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Kits</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item active" href="shop.html">Shop</a>
                        <a class="dropdown-item" href="learning.html">Learning</a>
                        <a class="dropdown-item" href="community.html">Community</a>
                        <a class="dropdown-item" href="sports.html">Sports</a>
                        <a class="dropdown-item" href="account.html">Account</a>
                        <a class="dropdown-item" href="music.html">Music</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Samples</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="inbox.html">Inbox</a>
                        <a class="dropdown-item" href="email.html">Email</a>
                        <a class="dropdown-item" href="chat.html">Chat</a>
                        <a class="dropdown-item" href="invoice.html">Invoice</a>
                        <a class="dropdown-item" href="appointments.html">Appointments</a>
                        <a class="dropdown-item" href="projects.html">Projects</a>
                        <a class="dropdown-item" href="clients.html">Clients</a>
                        <a class="dropdown-item" href="contact.html">Contact</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Components</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="ui-button.html">Button</a>
                        <a class="dropdown-item" href="ui-dropdown.html">Dropdown</a>
                        <a class="dropdown-item" href="ui-card.html">Card</a>
                        <a class="dropdown-item" href="ui-card-reveal.html">Card Reveal</a>
                        <a class="dropdown-item" href="ui-tab.html">Tab</a>
                        <a class="dropdown-item" href="ui-tree.html">Tree</a>
                        <a class="dropdown-item" href="ui-nestable.html">Nestable</a>
                        <a class="dropdown-item" href="ui-notifications.html">Notifications</a>
                        <a class="dropdown-item" href="ui-material-icons.html">Material Icons</a>
                        <a class="dropdown-item" href="ui-picto-icons.html">Picto Icons</a>
                        <a class="dropdown-item" href="ui-progress.html">Progress</a>
                        <a class="dropdown-item" href="ui-forms.html">Forms</a>
                        <a class="dropdown-item" href="ui-file-upload.html">File Upload</a>
                        <a class="dropdown-item" href="ui-spinner.html">Spinner</a>
                        <a class="dropdown-item" href="ui-tables.html">Tables</a>
                        <a class="dropdown-item" href="ui-charts.html">Charts</a>
                        <a class="dropdown-item" href="ui-calendar.html">Calendar</a>
                    </div>
                </li>
            </ul>
 -->        </div>
    </div>
    <!-- END upper navbar -->

    <div class="mdk-drawer-layout mdk-js-drawer-layout" fullbleed push responsive-width="992px">
        <div class="mdk-drawer-layout__content">
            <!-- header-layout -->
            <div class="mdk-header-layout mdk-js-header-layout" has-scrolling-region>

                <!-- header -->

                <div class="mdk-header mdk-js-header mdk-header--app mdk-header--inverse bg-primary m-0" effects="waterfall" fixed>
                    <div class="mdk-header__content">
                        <div class="navbar navbar-inverse" primary>
                            <div class="container-fluid pl-0">
                                <button class="navbar-toggler" data-toggle="sidebar" type="button">
                                  <span class="material-icons">menu</span>
                                </button>

                                <div class="d-flex flex align-items-center">

                                <h1 class="mdk-header_title flex">Admin</h1>

                                    <div class="navbar-nav nav">
                                        <div class="nav-item dropdown dropdown-notifications dropdown-full-xs">
                                            <button class="nav-link btn-flush dropdown-toggle" type="button" data-toggle="dropdown" data-caret="false">
                                              <i class="material-icons">notifications</i>
                                              <!-- <span class="badge badge-dark">3</span> -->
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <div class="list-group list-group-flush">
                                                    <!-- <a href="#" class="list-group-item list-group-item-action">
                                                        <small class="text-muted list-group-item-meta">3 min ago</small>
                                                        <span class="media">
                                                            <span class="media-left align-self-center">
                                                            <i class="material-icons md-2 icon--left text-danger">account_circle</i>
                                                            </span>
                                                            <span class="media-body align-self-center">
                                                            <p class="mb-1 lh-1">Your profile information has not been synced correctly.</p>
                                                            </span>
                                                        </span>
                                                    </a> -->
                                                    <a href="#" class="list-group-item list-group-item-action">nothing</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="nav-item dropdown">
                                            <button class="btn btn-secondary border-0 dropdown-toggle " type="button" data-toggle="dropdown"><i class="material-icons m-1" >settings</i></button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">nothing</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- content -->
                <div class="mdk-header-layout__content page-content">
                    <ol class="breadcrumb hidden-print breadcrumb--large container-fluid--large">
                        @stack('Tracks')
                    </ol>
                    @yield('content')
                </div>
                <!-- END CONTENT -->
            </div>

            <!-- App Settings FAB -->
            <!-- <div data-fm-app-settings-fab-vm>
                <fm-app-settings-fab :themes="[
                { id: 'default' },
                { id: 'teal' },
                { id: 'red' },
                { id: 'purple' },
                { id: 'minimal' },
                { id: 'dark' }
                ]" :theme-config="{
                'default': {
                '#default-drawer .sidebar': {
                addClass: ['sidebar-dark'],
                removeClass: ['sidebar-light', 'bg-white']
                },
                '#default-drawer .sidebar-brand-icon': {
                src: '/images/admin/logo.svg'
                }
                },
                'minimal': {
                '#default-drawer .sidebar': {
                addClass: ['sidebar-light', 'bg-white'],
                removeClass: ['sidebar-dark']
                },
                '#default-drawer .sidebar-brand-icon': {
                src: '/images/admin/logo-dark.svg'
                }
                },
                'dark': {
                '#default-drawer .sidebar': {
                addClass: ['sidebar-dark'],
                removeClass: ['sidebar-light', 'bg-white']
                },
                '#default-drawer .sidebar-brand-icon': {
                src: '/images/admin/logo.svg'
                },
                }
                }">
                </fm-app-settings-fab>
            </div> -->
        </div>
        <!-- // END drawer-layout__content -->

        <!-- drawer -->
        <div class="mdk-drawer mdk-js-drawer" id="default-drawer">
            <div class="mdk-drawer__content">
                <div class="mdk-drawer__inner sidebar sidebar-dark sidebar-left d-flex flex-column o-hidden">
                    <div class="sidebar-p-x sidebar-b-b">

                    <!-- Brand -->
                        <a href="#" class="sidebar-brand">
                            <img class="sidebar-brand-icon" src="{{ URL::asset('public/assets/images/admin/logo.svg') }}" width="26" alt="AdminDash">
                            AdminDash
                        </a>
                    <!-- // END Brand -->

                    <!-- Search -->
                        <form class="sidebar-search-form sidebar-m-b">
                            <input type="text" class="form-control" placeholder="Search">
                            <button class="btn" role="button" type="button"><i class="material-icons">search</i></button>
                        </form>
                    <!-- // END Search -->
                    </div>

                    <div class="flex p-relative">
                        <div data-simplebar data-simplebar-force-enabled="true" class="fullbleed">

                            <div class="sidebar-heading sidebar-m-t">MENU</div>
                            <ul class="sidebar-menu">
                                <li class="sidebar-menu-item @stack('classhomesidebar1') ">
                                    <a class="sidebar-menu-button" data-toggle="collapse" href="#collapsehome">
                                    <span class="badge badge-primary sidebar-menu-badge"><!--3--></span><i class="material-icons sidebar-menu-icon">dashboard</i>Dashboard</a>
                                    <ul class="sidebar-submenu collapse @stack('classhomesidebar2') sm-indent" id="collapsehome">
                                        <li class="sidebar-menu-item @stack('classhome')">
                                            <a class="sidebar-menu-button" href="{{url('admin/dashboard')}}"> Home</a>
                                        </li>
                                        <li class="sidebar-menu-item @stack('classfile')">
                                            <a class="sidebar-menu-button" href="{{url('admin/dashboard/file')}}"> File Manager</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="sidebar-menu-item @stack('classpagessidebar1')">
                                    <a class="sidebar-menu-button" data-toggle="collapse" href="#collapsepages">
                                    <span class="badge badge-primary sidebar-menu-badge"><!--3--></span><i class="material-icons sidebar-menu-icon">border_color</i>Topic</a>
                                    <ul class="sidebar-submenu collapse @stack('classpagessidebar2') sm-indent" id="collapsepages">
                                        <li class="sidebar-menu-item @stack('classpages')">
                                            <a class="sidebar-menu-button" href="{{url('admin/topic')}}"> List</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="sidebar-user sidebar-b-t dropup">
                        <a href="#" class="sidebar-text flush-underline media dropdown-toggle" data-toggle="dropdown">
                            <span class="media-left">
                                <img src="{{ URL::asset('public/assets/images/admin/people/110/guy-5.jpg') }}" width="45" alt="" class="rounded-circle">
                            </span>
                            <span class="media-body align-self-center">
                                <strong class="sidebar-link"><?= Auth::user()->name ?></strong><br/>
                                admin
                            </span>
                        </a>
                        <div class="sidebar-m-x dropdown-menu dropdown-menu-full dropdown-menu-caret-center">
                            <!-- <a href="#" class="dropdown-item">Account</a>
                            <a href="#" class="dropdown-item">View History</a>
                            <a href="#" class="dropdown-item">Payments</a>
                            <a href="#" class="dropdown-item text-danger">Logout</a> -->
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // END drawer -->

    </div>
    <!-- // END drawer-layout -->
    
    <!-- jQuery -->
    @stack('scripts')
    <!-- Bootstrap -->
    <script src="{{ URL::asset('public/assets/vendor/admin/tether.min.js') }}"></script>
    <script src="{{ URL::asset('public/assets/vendor/admin/bootstrap.min.js') }}"></script>

    <!-- Simplebar -->
    <!-- Used for adding a custom scrollbar to the drawer -->
    <script src="{{ URL::asset('public/assets/vendor/admin/simplebar.js') }}"></script>

    <!-- DOM Factory -->
    <!-- MDK and Card Overlay Component dependency -->
    <script src="{{ URL::asset('public/assets/vendor/admin/dom-factory.js') }}"></script>

    <!-- MDK -->
    <script src="{{ URL::asset('public/assets/vendor/admin/material-design-kit.js') }}"></script>


    <script src="{{ URL::asset('public/assets/vendor/admin/bootstrap-datepicker.min.js') }}"></script>
    <script>
        (function() {
            'use strict';

            $('.datepicker').datepicker();

        })()
    </script>

    <!-- Moment.js -->
    <script public/="{{ URL::asset('public/assets/vendor/admin/moment.min.js') }}"></script>
    <script src="{{ URL::asset('public/assets/vendor/admin/moment-range.min.js') }}"></script>

    <!-- Theme Colors -->
    <script src="{{ URL::asset('public/assets/js/admin/colors.js') }}"></script>

    <!-- Chart.js -->
    <script src="{{ URL::asset('public/assets/vendor/admin/Chart.min.js') }}"></script>

    <!-- Chart.js Settings -->
    <script src="{{ URL::asset('public/assets/js/admin/chartjs-settings.js') }}"></script>

    <!-- Chart.js Helper Class -->
    <script src="{{ URL::asset('public/assets/js/admin/chartjs-element.js') }}"></script>

    <!-- Mini Bar Chart (Chart.js) -->
    <script src="{{ URL::asset('public/assets/js/admin/chartjs-mini-bar.js') }}"></script>

    <!-- Day Visitors by Hour Bar Chart (Chart.js) -->
    <script src="{{ URL::asset('public/assets/js/admin/chartjs-day-bar.js') }}"></script>

    <!-- Month Sales by Day Line Chart (Chart.js) -->
    <script src="{{ URL::asset('public/assets/js/admin/chartjs-month-line.js') }}"></script>

    <!-- Year Sales by Month Bar Chart (Chart.js) -->
    <script src="{{ URL::asset('public/assets/js/admin/chartjs-year-bar.js') }}"></script>


    <script>
        (function() {
            'use strict';

            // Connect button(s) to drawer(s)
            var sidebarToggle = Array.prototype.slice.call(document.querySelectorAll('[data-toggle="sidebar"]'))

            sidebarToggle.forEach(function(toggle) {
                toggle.addEventListener('click', function(e) {
                    var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
                    var drawer = document.querySelector(selector)
                    if (drawer) {
                        drawer.mdkDrawer.toggle()
                    }
                })
            })

            var drawerNode = document.querySelector('.mdk-drawer')
            var headerNode = document.querySelector('.mdk-header')
            var headerLayoutNode = document.querySelector('.mdk-header-layout')

            if (drawerNode) {
                drawerNode.addEventListener('mdk-drawer-changed', function() {
                    headerNode.mdkHeader._reset()
                })
            }

            var willShrink = document.querySelectorAll('.will-shrink-to-hidden')
            var willFade = document.querySelectorAll('.will-fade')
            var scrollingRegion = [document.querySelector('.mdk-header-layout__content'), window]

            if (headerNode && (willShrink || willFade)) {
                scrollingRegion.forEach(function(scrollingRegion) {
                    scrollingRegion.addEventListener('scroll', function() {
                        var progress = headerNode.mdkHeader.getScrollState().progress

                        Array.prototype.slice.call(willShrink).forEach(function(willShrink) {
                            willShrink.classList.toggle('shrink-to-hidden', progress > 0.1)
                        })

                        Array.prototype.slice.call(willFade).forEach(function(willFade) {
                            willFade.style.opacity = 1 - progress
                        })
                    })
                })
            }

            // TOOLTIPS
            $('[data-toggle="tooltip"]').tooltip()
            $('[data-toggle="popover"]').popover()

            // SIDEBAR COLLAPSE MENUS
            $('.sidebar .collapse').on('show.bs.collapse', function(e) {
                $(this).closest('.sidebar-menu').find('.open').find('.collapse').collapse('hide');
                $(this).closest('li').addClass('open');
            });
            $('.sidebar .collapse').on('hidden.bs.collapse', function(e) {
                $(this).closest('li').removeClass('open');
            });

            // DISMISSABLE DROPDOWN
            $('body').on({
                'shown.bs.dropdown': function() {
                    this.closable = false
                },
                'click': function(e) {
                    var selectors = ['[data-dismiss="dropdown"]', '[data-toggle="dropdown"]']
                    selectors.forEach(function(selector) {
                        if (!this.closable) {
                            this.closable = $(e.target).is(selector) || $(e.target).closest(selector).length > 0
                        }
                    }, this)
                },
                'hide.bs.dropdown': function(e) {
                    var selector = ['[data-toggle="dropdown"]']
                    if (!this.closable) {
                        this.closable = $(e.relatedTarget).is(selector) || $(e.relatedTarget).closest(selector).length > 0
                    }
                    return this.closable === true
                }
            }, '[data-dropdown-dismissable]')

            ///////////////
            // PRELOADER //
            ///////////////

            window.addEventListener('load', function() {
                $('.preloader').fadeOut()
            })

            //////////////////////////////////////////
            // BREAK OUT OF ENVATO LIVE DEMO IFRAME //
            //////////////////////////////////////////

            window.top.location.hostname !== window.location.hostname && (window.top.location = window.location)

        })()
    </script>

    <!-- Vue.js (safe to remove) -->
    <!-- Used for App Settings -->
    <script src="{{ URL::asset('public/assets/vendor/admin/vue.min.js') }}"></script>

    <!-- Cookies (safe to remove) -->
    <!-- Used for App Settings -->
    <script src="{{ URL::asset('public/assets/vendor/admin/js.cookie.js') }}"></script>

    <!-- App Settings (safe to remove) -->
    <script src="{{ URL::asset('public/assets/vendor/admin/fm-app-settings.js') }}"></script>


    <script>
        $(document).ready(function() {
            var table = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
                
            } );
         
            table.buttons().container()
                .appendTo( $('div.eight.column:eq(0)', table.table().container()) );
        } );
    </script>

<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.4/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script> -->
</body>


<!-- Mirrored from admindash.themekit.io/shop.html by HTTrack Website Copier/3.x [XR&CO'2010], Sun, 25 Jun 2017 19:54:55 GMT -->
</html>