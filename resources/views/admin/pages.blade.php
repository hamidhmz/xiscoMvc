@extends('admin.layout.layout')
@section('title', 'Dashboard')
@push('style')

@endpush
@push('Tracks')
<li class="breadcrumb-item"><a href="{{url('admin/topic')}}">Pages</a></li>
<li class="breadcrumb-item active">Topic</li>
@endpush
@section('content')
<div class="card">
    <div class="card-block">
        <fieldset class="form-group">   
            <div >
                <table id="example" class="ui celled table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Position</th>
                            <th>Text</th>
                            <th>Caption1</th>
                            <th>Caption2</th>
                            <th>Language</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Position</th>
                            <th>Text</th>
                            <th>Caption1</th>
                            <th>Caption2</th>
                            <th>Language</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    <?php foreach($pages as $page){ $x = (position_echo($page->position)) ;$y = (str_limit($page->text, 15));?>
                        <tr>
                            <td><?= $page->id ?></td>
                            <td><div style="direction: rtl;text-align: right"><?= $page->position ?></div></td>
                            <td><div style="direction: <?php if($page->lan=='fa'){echo 'rtl';}else{echo 'ltr';}?>;text-align: <?php if($page->lan=='fa'){echo 'right';}else{echo 'left';}?>">{{ $y }}</div></td>
                            <td><?= specify_align($page->caption1,$page->lan) ?></td>
                            <td><?= specify_align($page->caption2,$page->lan) ?></td>
                            <td><?= $page->lan ?></td>
                            <td><a href="topic/edit/{{$page->id}}"><i class="material-icons m-1" style="font-size:20px;color: #4a90e2">mode_edit</i></a></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
</div>
@endsection
@push('classpages')
active
@endpush
@push('classuppages')
active
@endpush
@push('classpagessidebar1')
 active open
@endpush
@push('classpagessidebar2')
show 
@endpush
@push('scripts')

    <script src="{{ URL::asset('public/assets/vendor/admin/jquery.min.js') }}"></script>
    <script src="/code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.semanticui.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.semanticui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"></script>

@endpush
