@extends('admin.layout.layout')
@section('title', 'Dashboard')
@push('Tracks')
<li class="breadcrumb-item"><a href="{{url('admin/topic')}}">Pages</a></li>
<li class="breadcrumb-item"><a href="{{url('admin/topic')}}">Topic</a></li>
<li class="breadcrumb-item active">Edit</li>
@endpush
@section('content')
<div class="card">
	<div class="card-block">
		<fieldset class="form-group">	
			<label >Title</label>
			<input id="title"  class="form-control " value="{{$Pages->caption1}}" >
		</fieldset>
		<fieldset class="form-group">	
			<label >Caption</label>
			<input id="caption" class="form-control " value="{{$Pages->caption2}}" >
		</fieldset>
		<fieldset class="form-group">
			<div class="adjoined-bottom" style="background: #f9f9f9">
				<div class="grid-container">
					<div class="grid-width-100" style="padding: 0px">
						<div id="editor">
							<?= $Pages->text ?>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<button id="submit" class="submitl">Publish</button>
		</fieldset>
	</div>
</div>
@endsection
@push('classpages')
active
@endpush
@push('classuppages')
active
@endpush
@push('classpagessidebar1')
 active open
@endpush
@push('classpagessidebar2')
show 
@endpush
@push('scripts')
	<script src="{{ URL::asset('public/assets/vendor/admin/jquery.min.js') }}"></script>

	<script type="text/javascript">
	    $(document).ready(function(){
			$('.submitl').click(function(){
				$('#submit').html('');
				$('#submit').removeClass('submitl');    
				$('#submit').addClass('loader');
				var btn1="submit";
		        var text=CKEDITOR.instances.editor.getData();
		        var title=$('#title').val();
		        var caption=$('#caption').val();
		        var id=<?= $Pages->id ?>;
				$.ajax({
			        method: 'get',
			        url: "{{url('admin/topic/ajax')}}",
			        data: {btn1:btn1,id:id,caption:caption,title:title,text:text},
			        async: true,
			        success:
			        function(){
			        	$('#submit').removeClass('loader');
			        	$('#submit').addClass('successl');
			        	$('#submit').removeClass('submitl');
                		$('#submit').html('<i class=material-icons m-1>check</i>')
                		}
			        ,
			        error: function(data){
			            console.log(data);
			            alert("fail" + ' ' + this.data)
			        },
			    });
	        });
	    });
    </script>
	<script>
		initSample();
	</script>
@endpush
@push('style')
	<script src="{{ URL::asset('public/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ URL::asset('public/ckeditor/samples/js/sample.js') }}"></script>
	<link rel="stylesheet" href="{{ URL::asset('public/ckeditor/samples/css/samples.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('public/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@endpush