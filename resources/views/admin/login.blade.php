<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  <link type="text/css" href="{{ URL::asset('public/assets/css/admin/login.css') }}" rel="stylesheet">
  <link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

</head>
<div id="particles-js"></div>
<body class="login">
  <div class="container">
    <div class="login-container-wrapper clearfix">
      <div class="logo">
        <i class="fa fa-sign-in"></i>
      </div>
      <div class="welcome"><strong>Welcome,</strong> please login</div>

      <form class="form-horizontal login-form" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}
        <div class="form-group relative">
          <input  id="login_username" name="name" class="form-control input-lg"  placeholder="Username" required>
          <i class="fa fa-user"></i>
        </div>
        <div class="form-group relative password">
          <input name="password" id="login_password" class="form-control input-lg" type="password" placeholder="Password" required>
          <i class="fa fa-lock"></i>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-success btn-lg btn-block">Login</button>
        </div>
        <div class="checkbox pull-left">
          <label><input type="checkbox"> Remember</label>
        </div>
        <div class="checkbox pull-right">
          
        </div>
      </form>
    </div>
    
    <h4 class="text-center">
        
    </h4>
  </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js" ></script>
    <script type="text/javascript" src="{{ URL::asset('public/assets/js/admin/stats.js') }}" ></script>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  </body>
  </html>