@extends('admin.layout.layout')
@section('title', 'File Manager')
@push('Tracks')
<li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
<li class="breadcrumb-item active">File</li>
@endpush
@push('classfile')
active
@endpush
@push('classupfile')
active
@endpush
@section('content')
<div class="card">
	<div class="card-block">
		<fieldset class="form-group">
			<div id="elfinder"></div>
		</fieldset>
	</div>
</div>
@endsection
@push('styles')
<!-- jQuery UI (REQUIRED) -->
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">

<!-- elFinder CSS (REQUIRED) -->
<link rel="stylesheet" type="text/css" href="{{url('public/elfinder/css/elfinder.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/elfinder/css/theme.css')}}">
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!--<![endif]-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- elFinder JS (REQUIRED) -->
<script src="{{url('public/elfinder/js/elfinder.min.js')}}"></script>

<!-- Extra contents editors (OPTIONAL) -->
<script src="{{url('public/elfinder/js/extras/editors.default.js')}}"></script>

<!-- GoogleDocs Quicklook plugin for GoogleDrive Volume (OPTIONAL) -->
<!--<script src="js/extras/quicklook.googledocs.js"></script>-->

<!-- elFinder initialization (REQUIRED) -->
<script type="text/javascript" charset="utf-8">
	// Documentation for client options:
	// https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
	$(document).ready(function() {
		$('#elfinder').elfinder(
			// 1st Arg - options
			{
				cssAutoLoad : false,               // Disable CSS auto loading
				baseUrl : './',                    // Base URL to css/*, js/*
				url : "{{url('public/elfinder/php/connector.minimal.php')}}"  // connector URL (REQUIRED)
				// , lang: 'ru'                    // language (OPTIONAL)
			},
			// 2nd Arg - before boot up function
			function(fm, extraObj) {
				// `init` event callback function
				fm.bind('init', function() {
					// Optional for Japanese decoder "extras/encoding-japanese.min"
					delete fm.options.rawStringDecoder;
					if (fm.lang === 'jp') {
						fm.loadScript(
							[ fm.baseUrl + "{{url('public/elfinder/js/extras/encoding-japanese.min.js')}}" ],
							function() {
								if (window.Encoding && Encoding.convert) {
									fm.options.rawStringDecoder = function(s) {
										return Encoding.convert(s,{to:'UNICODE',type:'string'});
									};
								}
							},
							{ loadType: 'tag' }
						);
					}
				});
				// Optional for set document.title dynamically.
				var title = document.title;
				fm.bind('open', function() {
					var path = '',
						cwd  = fm.cwd();
					if (cwd) {
						path = fm.path(cwd.hash) || null;
					}
					document.title = path? path + ':' + title : title;
				}).bind('destroy', function() {
					document.title = title;
				});
			}
		);
	});
</script>
@endpush
@push('classhomesidebar1')
 active open
@endpush
@push('classhomesidebar2')
show 
@endpush