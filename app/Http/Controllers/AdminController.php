<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages;
use App;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }
    
    public function file()
    {
        return view('admin.file');
    }
    public function pages()
    {
        $pages = Pages::all();
        return view('admin.pages',compact('pages'));
    }
    public function edit($id)
    {
        $Pages = Pages::find($id);
        return view('admin.edit',compact('Pages'));
    }
    public function ajax(Request $Request)
    {
        $page = Pages::where('id' , $Request->id)->first();
        $page->caption1 = $Request->title;
        $f = $Request->text;
        $page->text = $f;
        $page->caption2 = $Request->caption;
        $page->save();
    }
    public function login_form()
    {
        if ( Auth::check() && Auth::user()->role=='admin' ){
            return redirect('admin/dashboard');
        }else{
            return view('admin.login');
        }
    }
    public function home($locale='fa')
    {
        App::setLocale($locale);
        $texts = Pages::all();
        return view('home',compact('texts'));
    }
}
