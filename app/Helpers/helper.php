<?php
	function text_echo($x, $length){
	  if(strlen($x)<=$length)
	  {
	    return $x;
	  }
	  else
	  {
	    $y=substr($x,0,$length) . '...';
	    return $y;
	  }
	}
	function position_echo($x){
	  if ($x == 1) {
	  	return 'الین متن و متن معرفی سایت.';
	  }elseif ($x == 2) {
	  	return 'متن شاپی سافت.';
	  }elseif ($x == 3) {
	  	return 'متن xisco.';
	  }elseif ($x == 4) {
	  	return 'متن tmpiran.';
	  }elseif ($x == 5) {
	  	return 'متن avada.';
	  }elseif ($x == 6) {
	  	return 'متن معرفی میلاد.';
	  }elseif ($x == 7) {
	  	return 'متن معرفی حمید.';
	  }else{
	  	return 'تعریف نشده.';
	  }
	}
	function specify_align($text,$language){
		if($language == 'fa'){
			return '<div style="direction: rtl;text-align: right">'.$text.'</div>';
		}elseif ($language == 'en') {
			return $text;
		}else{
			return 'ثبت نشده';
		}
	}
	function textecho($id,$texts,$locale){
		if ($locale == 'fa'){
			return $texts->where('id',$id)->first()->text;
		}else{
			return $texts->where('id',$id)->first()->text;
		}
	}